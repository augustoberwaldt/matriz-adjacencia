#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FilePath  "matriz.txt"

int linha   = 0,
    coluna  = 0,
    tamanho = 0;

int** grafo;


struct Vertice{
   int v1;
   int v2;
};

struct Vertice  vertices;
/**
 * funcao responsavel por calcular o tamanho do arquivo
 * @param File arquivo
 * @return int
 */
int sizeFile(FILE *arquivo)
{
    long posicaoAtual = ftell(arquivo);
    long tamanho;
    fseek(arquivo, 0, SEEK_END);
    tamanho = ftell(arquivo);
    fseek(arquivo, posicaoAtual, SEEK_SET);
    return (int) tamanho;
}
/**
 *  Le do arquivo
 */
void  readFile()
{
   FILE *arq = fopen(FilePath, "r");
   tamanho   = sizeFile(arq);

   char *Linha = (char *) malloc(sizeof(char*)  * tamanho);

   grafo   =  (int **) malloc(sizeof(int**) * (tamanho));

   char *result;

   if (!arq) {
        printf("\n Erro na abertura do arquivo.\n");
        exit (1);
   }

   while (!feof(arq)) {
      result = fgets(Linha, 100, arq);
      if (!result){
      	continue;
	  }
	  grafo[linha] = (int *) malloc((sizeof(int*) * (int) tamanho));
	  coluna = 0;
	  while (*result != '\0') {
		if (*result != '\n' && *result != ' ') {
		    char tmp  = *result;
		    grafo[linha][coluna]  =  atoi(&tmp);
		}
		result++;
		coluna++;
      }
      linha++;
   }

   fclose(arq);
}

/**
 *  funcao verificar se um vertice esta ligado a outro.
 *
 * @param int v1
 * @param int v2
 * @return int | caso  possua ligacao retorna 1 senao retorna 0
 */
int   isConnected(int v1, int v2)
{
    if (grafo[v1][v2] == 1) {
        return 1;
    }
    return 0;
}

/**
 *  funcao para imprimir o grafo.
 */
void print_g(int** g)
{
  int a = 0;
  int b = 0;
  for (a = 0 ;  a < linha; a++ ) {
	  for (b = 0 ;  b < coluna; b++) {
	    printf("%d", g[a][b]);
	  }
	  printf("\n");
  }

}

void doSearchDFS()
{

	 int w = 0;
	 int z = 0;
     int** mat = (int**) malloc(sizeof(int**)* linha);

     for (w = 0 ;  w < linha; w++) {
	    mat[w] = (int*) malloc(sizeof(int*)* linha);
	    for (z = 0 ;  z < coluna; z++) {
           mat[w][z] = (int*) malloc(sizeof(int*) * 3);
	       mat[w][z] = -1;
	       mat[w][z] = -1;
	       mat[w][z] = -1;
		}
	 }

	 for (w = 0 ;  w < linha; w++) {
	    for (z = 0 ;  z < coluna; z++) {
	       if (grafo[w][z]==1) {
	 			 mat[w][z] = w;
				 mat[w][z] = z;
				 mat[w][z] = z;
		   }
        }
	 }

     int ab =0;
     int cd = 0;


     int** pilha = (int**) malloc(sizeof(int**)* linha);
     int ini  = 0;
     int ini2 = 0;

     for (ini = 0; ini < linha; ini++){
        pilha[ini]  = (int*) malloc(sizeof(int*)* coluna);
        for (ini2 = 0; ini2 < coluna; ini2++){
            pilha[ini][ini2] = 0;
         }
     }

     int v1 = (vertices.v1 - 1);
     int v2 = (vertices.v2 - 1);
     int flag = 0;
     for(ab =0;  ab < linha; ab++) {
        for( cd = 0;  cd < coluna; cd++ ) {
           int value = mat[v1][cd];
           if (v1 == cd){
                continue;
           }
           printf("Passei: [%d][%d]\n", v1, cd);
           if (value == v2) {
             flag = 1;
             break;
           } else {
              if (value == -1) {
                continue;
              }
              v1 = mat[value][cd];

              if (v1 == -1){
                v1 = ab;
                continue;
              }
           }
        }
        if (flag){
             break;
        }
     }
    if (flag) {
       printf("\n Pode chegar");
    } else {
       printf("\n N�o Pode chegar");
    }

}
/** implementar futuramente*/
void doSearchBFS()
{



}
/**
 * search
 * @param int type
 */
void search(int type)
{

    switch (type) {
       case 1:
	       doSearchDFS();
	   break;
	   case 2:
	        doSearchBFS();
	   break;
	}

}


/**
 *  funcao inicial , inicializa o programa.
 */
void  start()
{
  readFile();
  print_g(grafo);

}




